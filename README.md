# 📗 Table of Contents

- [📖 About the Project](#about-project)
  - [🛠 Built With](#built-with)
    - [Tech Stack](#tech-stack)
    - [Key Features](#key-features)
- [💻 Getting Started](#getting-started)
  - [Setup](#setup)
  - [Prerequisites](#prerequisites)
  - [Install](#install)
  - [Usage](#usage)
- [👥 Authors](#authors)
- [🔭 Future Features](#future-features)
- [🤝 Contributing](#contributing)
- [⭐️ Show your support](#support)
- [🙏 Acknowledgements](#acknowledgements)
- [❓ FAQ (OPTIONAL)](#faq)
- [📝 License](#license)

<!-- PROJECT DESCRIPTION -->

# 📖 Bank Frontend <a name="about-project"></a>


Welcome to **Bank Frontend** site, your gateway to exploring my digital banking.

## 🛠 Built With <a name="built-with"></a>

### Tech Stack <a name="tech-stack"></a>

<details>
  <summary>Client</summary>
  <ul>
    <li><a href="https://react.dev/">React</a></li>
  </ul>
</details>

<details>
  <summary>Server</summary>
  <ul>
    <li><a href="https://vitejs.dev/">ViteJS</a></li>
  </ul>
</details>


<!-- Features -->

### Key Features <a name="key-features"></a>


- **Front end of a banking application**
- **React + Vite**
- **TailwindCSS**

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- GETTING STARTED -->

## 💻 Getting Started <a name="getting-started"></a>


To get a local copy up and running, follow these steps.

### Prerequisites

In order to run this project you need NodeJS installed in your machine.

### Setup

Clone this repository to your desired folder:


```sh

  git clone https://gitlab.com/microgenius/bank-frontend.git

  cd bank-frontend

```

### Install

Install dependencies of this project with:


```sh

  cd bank-frontend

  npm install

```

### Usage

To run the project, execute the following command:

```sh

  npm run dev

```

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- AUTHORS -->

## 👥 Author <a name="authors"></a>


👤 **Sandeep Ghosh**

- GitHub: [@mailsg](https://github.com/mailsg)
- LinkedIn: [LinkedIn](https://www.linkedin.com/in/sandeep0912)


<!-- FUTURE FEATURES -->

## 🔭 Future Features <a name="future-features"></a>


- **General Banking Features**
- **Live Chat Help**
- **Cloud Deployment**

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- CONTRIBUTING -->

## 🤝 Contributing <a name="contributing"></a>

Contributions, issues, and feature requests are welcome!

Feel free to check the [https://gitlab.com/microgenius/bank-frontend/issues](../../issues/).

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- SUPPORT -->

## ⭐️ Show your support <a name="support"></a>


If you like this project, care to leave a star and connect with me on social. Also,you are welcome to contribute to this project.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- ACKNOWLEDGEMENTS -->

## 🙏 Acknowledgments <a name="acknowledgements"></a>

I would like to thank the Open Source community for providing resources free of cost to make this project take shape.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- LICENSE -->

## 📝 License <a name="license"></a>

This project is [MIT](./MIT.md) licensed.

<p align="right">(<a href="#readme-top">back to top</a>)</p>
